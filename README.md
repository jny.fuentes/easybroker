# easybroker

Projecto para listar los titulos de las propiedades de easybroker

## Python Version
`3.10.6`

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install crehana-teams.

```bash
pip install -r requirements.txt
```
## How to run?
```bash
python properties/property_easybroker.py
```


## How to run test?
```bash
python -m unittest -v test
```

