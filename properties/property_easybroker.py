from typing import List
import logging
import requests
import sys
import json

logger = logging.getLogger()
logger.setLevel(logging.ERROR)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class  PropertyEasyBroker:
    def __init__(self) -> None:
        """ contructor """
    
    def print(self):
        """
            Print titles of properties
        """
        logger.info("starting the print")
        properties = self.get_properties()
        logger.info("get properties, count %s", len(properties))
        self.__print_only_titles(properties)
        logger.info("the titles were printed")
        logger.info("finishing the print")



    def get_properties(self)-> List[dict]:
        properties = []
        url_base = self.__get_url_base()
        token = self.__get_token()
        limit = self.__get_limit()
        page = 1
        #properties_data = []
        properties_data = self.__get_properties_data(url_base, page, limit, token)
        while self.__can_continue(properties_data):
            properties_content = self.__get_properties_content(properties_data)
            if properties_content:
                properties+=properties_content
            page += 1
            properties_data = self.__get_properties_data(url_base, page, limit, token)
            #if page >3 :
            #    break
            
        return properties
        
    def __print_only_titles(self,properties):
        """_summary_
           print only titles
        Args:
            properties (List): list propertie
        """
        if properties != None:
            for property in properties:
                title = self.get_title(property)
                self.__print_title(title)
        else:
            self.__print_text("Properties not found")
            logger.info("Properties not found")

    def __get_properties_data(self, url_base, page, limit, token):
        url = f"{url_base}?page={page}&limit={limit}"
        headers = {
            "accept": "application/json",
            "X-Authorization": token
        }

        #response = requests.get(self, url, headers=headers)
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            logger.error('response text: %s', response.text)
            logger.error('response status_code: %s', response.status_code)
            return None
        logger.debug(" content : %s", response.text)
        return json.loads(response.text)

    def __get_properties_content(self, properties_data):
        properties = None
        if "content" in properties_data and properties_data["content"]:
            properties = properties_data["content"]
        return properties

    def __can_continue(self, properties_data)->bool:
        is_valid = False
        # if properties_data and 
        if properties_data:
            if "content" in properties_data and properties_data["content"]:
                is_valid = True
        return is_valid

    def __get_url_base(self)->str:
        return "https://api.stagingeb.com/v1/properties"

    def __get_token(self)->str:
        return "l7u502p8v46ba3ppgvj5y2aad50lb9"

    def __get_limit(self):
        return 20
    

    def get_title(self,property)->str:
        title = None
        logger.debug("property: %s", property)
        if "title" in property and property['title']:
            title = property['title']
        return title
    def __print_title(self,title):
        if title:
            self.__print_text(title)
            logger.info(title)
        else:
            logger.info("text no printed")
    def __print_text(self,text):
        print(text)

def main():
    propertyEasyBroker = PropertyEasyBroker()
    propertyEasyBroker.print()

if __name__ == '__main__':
    main()