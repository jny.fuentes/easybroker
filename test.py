import unittest
from io import StringIO
from unittest.mock import patch
from properties.property_easybroker import PropertyEasyBroker


class TestPropertyEasyBroker(unittest.TestCase):

    @patch('sys.stdout', new_callable=StringIO)
    def test_print_all_properties(self,mock_out):
        """
        Test print all properties
        test 1
        """
        
        propertyEasyBroker = PropertyEasyBroker()
        PropertyEasyBroker.get_properties = self.mock_get_properties
        with patch('sys.stdout', new = StringIO()) as fake_out:
            propertyEasyBroker.print()
            self.assertEqual(fake_out.getvalue(), "TERRENO EN VENTA EN SAN AGUSTIN EN SAN PEDRO GARZA GARCIA\n")
    def test_print_title(self):
        """
        Test print title
        """
        data = { "title": "title 1"}
        propertyEasyBroker = PropertyEasyBroker()
        PropertyEasyBroker.get_properties = self.mock_get_properties
        result = propertyEasyBroker.get_title(data)
        self.assertEqual(result, "title 1")
    def custom_method2(*args, **kwargs):
        return True
    def mock_get_properties(*args, **kwargs):
        return [{
      "public_id": "EB-C6507",
      "title": "TERRENO EN VENTA EN SAN AGUSTIN EN SAN PEDRO GARZA GARCIA",
      "title_image_full": "https://assets.stagingeb.com/property_images/36507/127606/EB-C6507.jpg?version=1611968696",
      "title_image_thumb": "https://assets.stagingeb.com/property_images/36507/127606/EB-C6507_thumb.jpg?version=1611968696",
      "location": "Real de San Agustin, San Pedro Garza García, Nuevo León",
      "operations": [
        {
          "type": "sale",
          "amount": 1193250,
          "currency": "USD",
          "formatted_amount": "US$ 1,193,250",
          "commission": {
            "type": "percentage"
          },
          "unit": "total"
        }
      ],
    }]

if __name__ == '__main__':
    unittest.main()